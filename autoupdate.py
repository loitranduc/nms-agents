#--------------------------------------------------
# @author: Tran Duc Loi 
# @email: loitranduc@gmail.com
# @version: 1.0
# @project: NMS - Jetpjng.com
# @community: Pythonvietnam
#--------------------------------------------------
import requests
import subprocess
import os

# GIT_URL = "https://loitranduc@bitbucket.org/loitranduc/nms-agents.git"
VERSION_URL = "https://bitbucket.org/loitranduc/nms-agents/raw/47a8c3957811d0f13c3e7482d5deda37bf29b49e/.VERSION"

class VersionControl(object):
	"""docstring for VersionControl"""
	def __init__(self):
		super(VersionControl, self).__init__()
		self.current_version = 0
		self.new_version = 0
		self.current_dir = os.path.dirname(os.path.abspath(__file__))

	def getCurrentVersion(self):
		try:
			with open(".VERSION", "r") as fh:
				self.current_version = fh.read(1024)
				print("Current version: {0}".format(self.current_version))
				return True

		except Exception as e:
			print("Error while getting version: {0}".format(e))
			return False

	def getNewVersion(self):
		try:
			r = requests.get(VERSION_URL)
			if r.status_code == 200:
				self.new_version = r.text
				print("New version: {0}".format(self.new_version))
				return True
			else:
				# print(r.text)
				print("Error while geting new version: {0}".format(r))
				return False
		except Exception as e:
			print(e)
			return False

	def getCurrentCommit(self):
		try:
			print("Checking local version ...")
			p = subprocess.Popen("cd {0} ; git rev-parse HEAD".format(self.current_dir), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) 
			rt = p.wait()
			msg = p.stdout.read().decode("utf-8")
			# print(msg.strip())
			self.current_version = msg.strip()
			return True
		except Exception as e:
			print("Exception while get current version: {0}".format(e))
			return False


	def getRemoteCommit(self):
		try:			
			print("Checking update version ...")
			p = subprocess.Popen("cd {0} ; git ls-remote origin -h refs/heads/master".format(self.current_dir), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) 
			rt = p.wait()
			msg = p.stdout.read().decode("utf-8")
			# print(msg.strip())
			self.new_version = msg.strip().split()[0]
			return True
		except Exception as e:
			print("Exception while get current version: {0}".format(e))
			return False


	def pullUpdates(self):
		try:
			print("Begin update now ...")
			# get current dir
			current_dir = os.path.dirname(os.path.abspath(__file__))
			# pull now
			
			p = subprocess.Popen("cd {0} ; git pull origin master -f".format(current_dir), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) 
			rt = p.wait()
			msg = p.stdout.read().decode("utf-8")
			# print(msg)
			print("Update done successfully. New version will be applied next run.")
			return True

		except Exception as e:
			print("Update fail. Contact admin for support: {0}".format(e))
			return False

	def brain(self):
		if self.getCurrentCommit() and self.getRemoteCommit():
			if self.current_version != self.new_version:
				self.pullUpdates()
				# return True
			else:
				print("Nothing to update!")

			return True
		else:
			print("Unable to check for the version.")
			return False


if __name__ == '__main__':
	v = VersionControl()
	# v.getCurrentCommit()
	# v.getRemoteCommit()
	# v.pullUpdates()

	v.brain()

		



























