#--------------------------------------------------
# @author: Tran Duc Loi 
# @email: loitranduc@gmail.com
# @version: 1.0
# @project: NMS - Jetpjng.com
# @community: Pythonvietnam
#--------------------------------------------------

import pika
import json
import random
from time import gmtime, strftime
from pingit import PingAll
from httpcheck import HttpCheck
import os, sys, time
from autoupdate import VersionControl

class AppAgent(object):
    # dev
    # def __init__(self, usr='loitd', pwd='123456a@', host='localhost', port=5672, vhost='default'):
    # prod
    def __init__(self, usr='loitd', pwd='pwd123***fuk', host='188.166.183.66', port=5672, vhost='default'):

        self.is_init = False
        while not self.is_init:
            try:
                creds = pika.PlainCredentials(username=usr, password=pwd)
                params = pika.ConnectionParameters(host=host, port=port, virtual_host=vhost, credentials=creds)
                self.conn = pika.BlockingConnection(parameters=params)
                self.ch = self.conn.channel()
                print("[x] Done all init ...")

                # setup consume
                self.ch.queue_declare(queue='default', durable=False, exclusive=False)
                self.ch.basic_qos(prefetch_count=100)
                self.ch.basic_consume(
                    consumer_callback=self.on_consume,
                    queue='default',
                    no_ack=False,
                )

                print("[x] Waiting message ...")
                self.is_init = True
                # Begin consuming
                self.ch.start_consuming()
            except KeyboardInterrupt:
                self.conn.close()
                print("[x] Agent interrupted by users ...")
                sys.exit(0)
            except pika.exceptions.ConnectionClosed as e:
                self.conn.close()
                print("Exception while init: {0}".format(e))
                time.sleep(5)
            except Exception as e:
                self.conn.close()
                print("Exception while init: {0}".format(e))
                time.sleep(5)

    def __del__(self):
        try:
            self.conn.close()
        except Exception as e:
            print('Goodbye')


    def on_consume(self, ch, method, props, body):
        print("[B] -------------------------------------------------- [V2.0]")
        print("[x] Received corr_id: {0}".format(props.correlation_id))
        
        # print(body)
        body = str(body.decode('utf-8'))

        if body is None or body == 'null':
            ch.basic_ack(delivery_tag=method.delivery_tag)
            print("[x] Done ack for None object")
            return True
        
        print("[x] Received content: {0}".format(body))

        res = self.doping(body)

        # publish back to the reply queue
        ch.basic_publish(
            exchange='',
            routing_key=props.reply_to,
            properties=pika.BasicProperties(
                correlation_id=props.correlation_id,
            ),
            body=res,
        )

        print("[x] Done response")
        # ack to mark the queue
        ch.basic_ack(delivery_tag=method.delivery_tag)
        print("[x] Done ack")

        # Now exit
        #self.conn.close()


    def doping(self, msg):
        """
        The ping method
        :param msg:
        :return:
        """
        try:
            self._msg = json.loads(msg) # it shoulds be a list if ips
            print("Received msg now doping!")
            print(self._msg)

            # for demo
            # res = []
            # for el in self._msg:
            #     res.append({'i': el, 't': random.randint(0,100), 'c': strftime("%Y-%m-%d %H:%M:%S", gmtime()) })
            #########################
            # ver 1.1
                # self.ips = [{'type': 'ping',
                #               'ip': ['127.0.0.1', '127.0.0.1', ],}, 
                #             {'type': 'http',
                #               'ip': ['127.0.0.1:80', '127.0.0.1:80', ],
                #             }]

            # real
            for el in self._msg:
                if el['type'] == 'http':
                    h = HttpCheck()
                    res2 = h.run(el['ip'])
                    # res2 = []
                elif el['type'] == 'https':
                    h = HttpCheck()
                    res3 = h.run(el['ip'], 'https')
                    # res3 = []
                elif el['type'] == 'ping':
                    # processing ping
                    p = PingAll()
                    res1 = p.run(el['ip'])
                    # res1 = []
            #all necessary done
            res = res1 + res2 + res3
            return json.dumps(res)
        except Exception as e:
            # raise e
            print("There is exception in AGENT doping")
            print(e)
            print("Clear the fucking message now")
            return json.dumps([])



if __name__ == '__main__':
    v = VersionControl()
    v.brain()
    agent = AppAgent()
