#--------------------------------------------------
# @author: Tran Duc Loi 
# @email: loitranduc@gmail.com
# @version: 1.1
# @project: NMS - Jetpjng.com
# @community: Pythonvietnam
# Version 1.1:
# Add HTTP check
#--------------------------------------------------

import grequests
import time
from time import gmtime, strftime
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class HttpCheck(object):
    """docstring for HttpCheck"""
    def __init__(self):
        super(HttpCheck, self).__init__()
        pass

    def get_cur_time(self):
        # get the time based on local time of the server
        # return strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

        # print(time.localtime(time.time()))
        # print(time.gmtime())
        # print(datetime.utcnow())
        return strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
        
        # hn = timezone('Asia/Saigon')
        # localdt = hn.localize(datetime.now())
        # return localdt.strftime('%Y-%m-%d %H:%M:%S %Z%z')

    def run(self, targets, ctype='http', defaultdie=999):
        try:
            # timeout for connect and read
            rs = (grequests.head(u, timeout=(3.05, 20), verify=False) for u in targets)
            x = grequests.map(rs)
            print(x)
            # y = list((xi.status_code, defaultdie)[(xi is not None)] for xi in x)
            y = list((xi.status_code if xi is not None else defaultdie) for xi in x)
            # print(y)
            #
            rx = []
            for i in range(len(x)):
                a = {'ip': targets[i], 't': y[i], 'c': self.get_cur_time(), 'type': ctype, 'msg': ''}
                rx.append(a)

            # for debug
            # print(rx)
            return rx
        except Exception as e:
            print(e)
            
        
if __name__ == '__main__':
    for x in range(1,100):
        urls = [
            'https://www.heroku.com',
            'https://docs.oracle.com',
            'https://megacard.vn',
            'https://shipantoan.vn',
            # 'https://kennethreitz.com',
            'https://requestb.in',
            'https://github.com',
            'https://api.github.com',
            'https://mail.google.com/mail/u/0/#inbox',
            'https://www.facebook.com/',
            'https://www.youtube.com/dashboard?o=U',
            'https://angularjs.org/',
            'https://dribbble.com/victorerixon',
            'https://surge.sh/',
            'https://www.flickr.com/',
            'https://twitter.com/',
            'https://wordpress.org/',
            'https://apps.admob.com/#analyze',
            'https://ib.techcombank.com.vn/servlet/BrowserServlet#1',
            'https://picjumbo.com/sweet-morning/',
            'https://stocksnap.io/',
            'https://unsplash.com/',
            'https://topica.edu.vn/',

            
        ]
        h = HttpCheck()
        h.run(urls)