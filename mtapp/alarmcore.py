#--------------------------------------------------
# @author: Tran Duc Loi 
# @email: loitranduc@gmail.com
# @version: 1.0
# @project: NMS - Jetpjng.com
# @community: Pythonvietnam
#--------------------------------------------------

# import caller
import requests
import time
import json
import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(name)s] [%(levelname)s] [%(threadName)-8s] %(message)s')

tokenkey = '184143720:AAHsCHA7iasOEwP1UmngYZiwC0pBIYETnso'
GETME_URL = "https://api.telegram.org/bot{0}/getMe".format(tokenkey)
GETUPDATE_URL = "https://api.telegram.org/bot{0}/getUpdates".format(tokenkey)
SENDMSG_URL = "https://api.telegram.org/bot{0}/sendMessage".format(tokenkey)
USERNAME = "Jetpjngbot"
LOGIN_URL = "http://jetpjng.com/core/alarm/login/"
ALARM_URL = "http://jetpjng.com/core/alarm/data/"

class AlarmCore(object):
	"""docstring for AlarmCore"""
	def __init__(self):
		super(AlarmCore, self).__init__()
		self.cur_update_id = 0

	def getMe(self):
		try:
			logging.info("Geting: {0}".format(GETME_URL))
			r = requests.get(GETME_URL)
			if r.status_code == 200:
				rx = r.json()
				logging.info(rx)
				if rx['ok']:
					logging.info("return check ok")
					if rx['result']['username'] == USERNAME:
						logging.info("username check ok")
						return True
					else:
						logging.info("username check fail")
						return False
				else:
					logging.error("return check fail")
					return False
			else:
				logging.error("no 200")
				return False
		except Exception as e:
			raise e
			return False

	def getUpdates(self):
		try:
			logging.info("Geting: {0}".format(GETUPDATE_URL))
			r = requests.get(GETUPDATE_URL, {"offset": self.cur_update_id, "limit": 100})
			if r.status_code == 200:
				rx = r.json()
				logging.info(rx)
				if rx['ok']:
					logging.info("return check ok")
					if rx["result"] == []:
						logging.info("No new message here")
						return True

					# read all the message
					logging.info("Number of message: {0}".format(len(rx["result"])))
					for i in range(len(rx["result"])):
						logging.info("Processing message {0}".format(i))
						# extracting info 
						self.cur_update_id = int(rx["result"][i]["update_id"]) + 1
						msgtext = rx["result"][i]["message"]["text"]
						uid = rx["result"][i]["message"]["from"]["id"]
						# uname = rx["result"][0]["message"]["from"]["username"]
						fname = "{0} {1}".format(rx["result"][i]["message"]["from"]["first_name"], rx["result"][i]["message"]["from"]["last_name"])

						# begin parsing
						if msgtext.startswith("Hello"):
							# 
							user_token = msgtext.split()
							if len(user_token) != 2:
								logging.error("Wrong format")
								continue
								# return False

							user_token = user_token[1]

							if ("@" not in user_token) and (":" not in user_token):
								logging.error("Wrong format")
								continue
								# return False
							# correct format
							logging.info("Correct format!")
							self.sendMsg(uid, "Hello {0}. We have received your registration and will feedback soon. Thank you.".format(fname))
							# now send token
							loginrs = self.login(user_token, uid)
							if loginrs == True:
								# send success message
								self.sendMsg(uid, "Login successfully. We will send you alarms to this contact. Thank you.")
							else:
								self.sendMsg(uid, "Login fail with errors: {0}. Thank you.".format(loginrs))

							# return True
						else:
							logging.error("Wrong format!")
							# return False
					
					logging.info("Begin r2")
					r2 = requests.get(GETUPDATE_URL, {"offset": self.cur_update_id, "limit": 1})					
					# return at the end
					return True
				else:
					logging.error("return check fail")
					return False
			else:
				logging.error("no 200")
				return False
		except Exception as e:
			raise e
			return False

	def sendMsg(self, to, msg):
		try:
			logging.info("Geting: {0}".format(SENDMSG_URL))
			r = requests.get(SENDMSG_URL, {"chat_id": to, "text": msg})
			if r.status_code == 200:
				rx = r.json()
				logging.info(rx)
				logging.info("Message sent")
			else:
				logging.error("no 200")
				return False
		except Exception as e:
			raise e
			return False

	def login(self, token, nick):
		try:
			postdata = json.dumps({'token': token, 'nick': str(nick)})
			print("Begin log user in with postdata: {0}".format(postdata))
			r = requests.post(LOGIN_URL, postdata)
			if r.status_code == 200:
				rx = r.json()
				logging.info("Received: {0}".format(rx))
				# check reg status
				# Received: {'msg': '', 'status': True}
				# Received: {'msg': '', 'status': False}
				return True if rx["status"] else rx["msg"]
			else:
				print(r.status_code)
				print(r.text)
				logging.error("no 200")
				return False
		except Exception as e:
			logging.error("Login error: {0}".format(e))
			return False
		else:
			pass
		finally:
			pass

	def getAlarms(self):
		"""data = [
		{'email': '', 'token': '', 'nicknames': '', 'alarms':[{'IP': '', 'status': 0}, {'IP': '', 'status': 1}]}, 
		{'email': '', 'token': '', 'nicknames': '', 'alarms':[{'IP': '', 'status': 0}, {'IP': '', 'status': 1}]}, 
		]"""
		try:
			logging.info("Geting: {0}".format(ALARM_URL))
			r = requests.get(ALARM_URL)
			if r.status_code == 200:
				rx = r.json()
				rx = rx["data"]
				logging.info(rx)
				# no alarms
				if rx == []:
					logging.info("No alarms to sent!")
					return True
				# there is some alarms
				for el in rx:
					alldead = []
					allwake = []
					for ip in el["alarms"]:
						alldead.append(ip["address"]) if ip["status"] == 0 else allwake.append(ip["address"])
						# if ip["status"] == 0:
						# 	alldead.append(ip["address"])
						# else:
						# 	allwake.append(ip["address"])
					# now send sms
					if alldead != []:
						self.sendMsg(el["nicks"], "Dead IP(s): {0}\n *This message from jetpjng.com".format("-".join(alldead)))

					if allwake != []:
						self.sendMsg(el["nicks"], "Just get alive IP(s): {0}\n *This message from jetpjng.com".format("-".join(allwake)))

				# return
				logging.info("All alarm processing done!")
				return True

			else:
				logging.error("no 200")
				return False
		except Exception as e:
			raise e
			return False
		


if __name__ == '__main__':
	a = AlarmCore()
	# a.getMe()
	# a.login("a", "nick")
	while 1:
		a.getUpdates()
		a.getAlarms()
		logging.info("Begin sleeping ...")
		time.sleep(30)

	# a.sendMsg(183518555, "yes, fuck")

